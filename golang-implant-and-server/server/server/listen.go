package server

import (
	"fmt"
	"net"
)

func ListenOn(port string) {
	listener, _ := net.Listen("tcp", "127.0.0.1:"+port)
	fmt.Printf("[+] Listening on %s\n", port)
	go func() {
		conn, err := listener.Accept()
		defer listener.Close()
		if err != nil {
			fmt.Printf("[x] Error in connection: %s\n", err)
		}
		fmt.Printf("\n[+] Connection received from %s\n", conn.RemoteAddr().String())
		if err = registerNewTCPConnection(conn); err != nil {
			fmt.Printf("[x] Error registering implant; connection dropped\n\t%s\n", err)
			conn.Close()
		} else {
			fmt.Println("[+] Implant registered")
		}
		printPrompt()
	}()
}
