package enum

import (
	"fmt"
	"os"
	"os/user"
	"runtime"
)

func Platform() string {
	return runtime.GOOS
}

func Hostname() string {
	hostname, err := os.Hostname()
	if err != nil {
		return fmt.Sprintf("Error obtaining Hostname: %s", err)
	}
	return hostname
}

func Username() string {
	user, err := user.Current()
	if err != nil {
		return fmt.Sprintf("Error obtaining Username: %s", err)
	}
	return user.Username
}
