//+build !windows

package execute

import "os/exec"

func makeCmd(command string) (*exec.Cmd) {
    return exec.Command("/bin/sh", "-c", command)
}
