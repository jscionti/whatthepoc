package main

import (
	"./server"

	"fmt"
	"os"
)

func init() {
	if os.Getenv("SUDO_USER") == "" {
		fmt.Printf("[x] %s must be run with root permissions\n", os.Args[0])
		os.Exit(1)
	}
}

func main() {
	server.InitialiseImplantMap()
	server.Prompt()
}
