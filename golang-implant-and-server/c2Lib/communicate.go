package c2Lib

import (
	"encoding/gob"
	"io"
)

type Packet struct {
	Type byte
	//Encryption byte
	//MD5
	Data []byte
}

const (
	ERROR      byte = 0
	REGISTER   byte = 1
	REGISTERED byte = 2
	PING       byte = 253
	PONG       byte = 254
)

func Send(packet Packet, channel io.Writer) error {
	return gob.NewEncoder(channel).Encode(packet)
}

func Receive(channel io.Reader) (packet Packet, err error) {
	err = gob.NewDecoder(channel).Decode(&packet)
	return
}
