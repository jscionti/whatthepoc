//+build windows

package execute

import (
    "os/exec"
    "syscall"
)

func makeCmd(command string) (*exec.Cmd) {
    cmd := exec.Command("C:\\Windows\\System32\\cmd.exe", "/c", command)
    cmd.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
    return cmd
}
