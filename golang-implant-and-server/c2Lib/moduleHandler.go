package c2Lib

import (
	"errors"
	"reflect"
)

var Modules map[string]interface{}

func init() {
	Modules = map[string]interface{}{"ping": makePingPacket,
		"pong":       makePongPacket,
		"register":   makeRegisterPacket,
		"registered": makeRegisteredPacket}
}

func MakePacket(name string, params ...interface{}) (packet Packet, err error) {
	f := reflect.ValueOf(Modules[name])
	if len(params) != f.Type().NumIn() {
		err = errors.New("Incorrect number of parameters")
		return
	}
	in := make([]reflect.Value, len(params))
	for k, param := range params {
		in[k] = reflect.ValueOf(param)
	}
	packet = f.Call(in)[0].Interface().(Packet)
	return
}
