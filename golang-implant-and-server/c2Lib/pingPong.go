package c2Lib

func makePingPacket() (packet Packet) {
	packet.Type = PING
	packet.Data = []byte("ping")
	return
}

func makePongPacket() (packet Packet) {
	packet.Type = PONG
	packet.Data = []byte("pong")
	return
}
