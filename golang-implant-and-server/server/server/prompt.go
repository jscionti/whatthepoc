package server

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var selectedSession int
var promptPrefix string

func printPrompt() {
	fmt.Printf("%s> ", promptPrefix)

}

func Prompt() {
	for {
		printPrompt()
		input, _ := bufio.NewReader(os.Stdin).ReadString('\n')
		inputArgs := strings.Split(strings.TrimSpace(input), " ")
		switch inputArgs[0] {
		case "help":
			if len(inputArgs) == 1 {
				fmt.Printf(`Available commands:
help (command)       prints this help message; optional: print (command) usage
listen [port]        listen on [port]
select [num]         select session [num]
sessions             list connected sessions
ping                 ping the selected session
exit                 close sessions, exit
`)
			} else {
				switch inputArgs[1] {
				case "listen":
					fmt.Printf("listen: start listening on a given port\nusage:  listen [port]\n    eg. listen 666\n")
				case "select":
					fmt.Printf("select: select a connected session to interact with\nusage:  select [num]\n    eg. select 1\n")
				case "sessions":
					fmt.Printf("sessions: list connected sessions\n")
				case "ping":
					fmt.Printf("ping: ping the selected session\n")
				case "exit":
					fmt.Printf("exit: close all connected sessions, stop server and exit\n")
				default:
					fmt.Printf("Unknown command\n")
				}
			}
		case "listen":
			if len(inputArgs) == 2 {
				if n, err := strconv.Atoi(inputArgs[1]); err == nil && 1 <= n && n <= 65535 {
					ListenOn(inputArgs[1])
				} else {
					fmt.Println("[x] Invalid port number provided")
				}
			} else {
				fmt.Println("[x] Usage: listen [port]")
			}
		case "sessions":
			for session, implant := range AllImplants {
				fmt.Printf("[*] Session %d:\n%s", session, implant.ToString())
			}
		case "select":
			if len(inputArgs) == 2 {
				if n, err := strconv.Atoi(inputArgs[1]); err == nil {
					if _, ok := AllImplants[n]; ok {
						selectedSession = n
						fmt.Printf("[+] Session %v selected\n", selectedSession)
						promptPrefix = fmt.Sprintf("session %v ", selectedSession)
					} else {
						fmt.Println("[x] Session identifier not valid")
					}
				} else {
					fmt.Println("[x] session argument must be a number")
				}
			} else {
				fmt.Println("[x] Usage: select [session]")
			}
		case "ping":
			if selectedSession != 0 {
				AllImplants[selectedSession].send("ping")
			} else {
				fmt.Println("[x] A session must be selected first")
			}
		case "exit":
			for _, i := range AllImplants {
				if i.alive {
					i.close()
				}
			}
			os.Exit(0)
		case "":
			continue
		default:
			fmt.Println("[x] Unknown command")
		}
	}
}
