package execute

import "bytes"

func Run(command string) (result bytes.Buffer) {
    cmd := makeCmd(command)
    cmd.Stdout = &result
    cmd.Stderr = &result
    cmd.Run()
    return
}
